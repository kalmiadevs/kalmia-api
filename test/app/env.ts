import { AppEnvironment }          from '../../src/core';
import { ApiEnvironment, ApiMode } from '../../src/common/enums';

export class Environment extends AppEnvironment {
  mode = ApiMode.Test;
  type = ApiEnvironment.QA;
  serverPort = 3000;
  rootPath = process.cwd();
  storagePath = `/tmp/kalmia-api/storage`;
  logsPath = `/tmp/kalmia-api/storage/logs`;
}
