import { Environment } from './env';
import { AppBuilder }  from '../../src/core/app.builder';

describe('App', () => {
  it('Basic app init', async (done) => {
    const app = AppBuilder.create(new Environment())
      .setSilentInit()
      .build();
    await app.init();
    done();
  });

  it('App init', async (done) => {
    const app = AppBuilder.create(new Environment())
      .withExpress()
      .setSilentInit()
      .build();
    await app.init();
    done();
  });
});
