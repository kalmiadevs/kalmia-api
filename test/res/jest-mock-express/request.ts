import * as express from 'express';

function mockReq(): express.Request {
  const request = {
    headers: {},
    reset: resetMock
  };

  return request as any;
}

function resetMock() {

}

export = mockReq;
