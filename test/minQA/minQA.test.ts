import { AppBuilder } from '../../src/core/app.builder';
import { Container }  from 'typedi';
import { App }        from '../../src/core';
import { Component }  from '../../src/common/component';

export class TestService extends Component {
  constructor() {
    super(TestService.name);
  }
}

describe('Minimal QA ready instance', () => {
  it('AppBuilder Minimal QA', async (done) => {
    const app = await AppBuilder.initMinQA();
    const cApp = Container.get(App);
    expect(app).not.toBeUndefined();
    expect(cApp).not.toBeUndefined();
    expect(app).toBe(cApp);
    done();
  });

  it('Component class proper setup', () => {
    Container.set('TestService', new TestService());
    const svc: TestService = Container.get('TestService');

    expect(svc.app).toBe(Container.get(App));

    expect(svc).toHaveProperty('logger');
    expect(svc.logger).toHaveProperty('log');
  });
});
