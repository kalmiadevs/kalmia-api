# Kalmia api core

## Introduction
Kalmia-api is a framework for building efficient, scalable Node.js server-side applications.

Under the hood, kalmia-api makes use of Express.

## Requirements
- Git - `git` should be global command
- NodeJs (version 8.5 or up)
- Yarn
- Docker - `docker` should be global command
- Docker Compose - `docker-compose` should be global command
- MongoDB (version 3.6)

> **[info] Tip**
>
> You can use mongo via docker.

## Installation

First you will have to register Kalmia private NPM (https://npm.kalmia.si/).

```bash
$ npm adduser --registry  https://npm.kalmia.si
$ yarn config set registry https://npm.kalmia.si
$ yarn login --registry  https://npm.kalmia.si
```

Verify: you should see line `//npm.kalmia.si/:always-auth=true`
If it is set to `false` edit it to `true`.
```bash
$ cat ~/.npmrc
```

Verify: you should see line `registry "https://npm.kalmia.si"`
```bash
$ cat ~/.yarnrc
```

Install dependencies
```
cd <project_name>
yarn install
``` 

## Running the build
All the different build steps are orchestrated via [npm scripts](https://docs.npmjs.com/misc/scripts).
Npm scripts basically allow us to call (and chain) terminal commands via npm.
This is nice because most JavaScript tools have easy to use command line utilities allowing us to not need grunt or gulp to manage our builds.
If you open `package.json`, you will see a `scripts` section with all the different scripts you can call.
To call a script, simply run `npm run <script-name>` from the command line.
You'll notice that npm scripts can call each other which makes it easy to compose complex builds out of simple individual build scripts.
Below is a list of all the scripts this template has available:


| Npm Script | Description |
| ------------------------- | -----------------------------------------------------------------------------------------------  |
| `security`                | Do security check.   |
| `build`                   | Full build.                                                         |
| `watch`                   | Runs all watch task to rebuild on file change.                      |
| `test`                    | Runs tests using Jest test runner                                   |  
| `tslint`                  | Runs TSLint on project files                                        |                                   

## Using api

Install the starter project with **Git**:
```bash
$ git clone git@bitbucket.org:kalmiadevs/kalmia-api-seed.git project
$ cd project
$ yarn install
$ npm run start
```

... or start a new project from scratch with **Yarn**:
```bash
$ yarn add @kalmia/api @kalmia/typegoose typedi
```


## Mode info

You can get more info in [Kalmia web dev book](https://book.kalmia.si).