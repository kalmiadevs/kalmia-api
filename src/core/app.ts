import { Express }                        from 'express';
import { MongoBootstrap }                 from './bootstrap/mongo.bootstrap';
import { LoggerBootstrap }                from './bootstrap/logger.bootstrap';
import { ExpressBootstrap }               from './bootstrap/express.bootstrap';
import * as http                          from 'http';
import { AppConfig, getDefaultAppConfig } from './app.conf';
import { AppEnvironment }                 from './app.env';
import { LoggerFactoryService }           from '../common/services/logger/logger.service';
import { LoggerInstance }                 from 'winston';
import { SwaggerService }                 from '../common/services/swagger/swagger.service';
import { ErrorHandler }                   from './router/middlewares/error-handler';
import { RouteHandler }                   from './router';
import { RouterBootstrap }                from './bootstrap/router.bootstrap';
import { AccountingService }              from '../common/services/accounting/accounting.service';
import { AuthenticationService }          from '../common/services/authentication';
import { AuthorizationService }           from '../common/services/authorization/authorization.service';
import { Bootstrap }                      from './bootstrap/bootstrap';
import { sleep }                          from '../common/utils';
import { HealthService }                  from '../common/services/health/health.service';
import { JobBootstrap }                   from './bootstrap/job.bootstrap';
import { DashboardService }               from '../common/services/dashboard/dashboard.service';
import { ClientService }                  from '../common/services/client/client.service';
import { Db }                             from 'mongodb';
import { MongoService }                   from '../common/services/mongo';
import { StrategyBootstrap }              from './bootstrap';
import { ConnectorService }               from '../common/services/connector';

export class App {
  // WEB
  expressApp: Express;
  errorHandler: ErrorHandler;
  expressServer?: http.Server;

  // AAA
  authentication: AuthenticationService;
  authorization: AuthorizationService;
  accounting: AccountingService;
  connector: ConnectorService;

  // DB & CLIENT
  mainDb: Db;
  mongoService: MongoService;
  clientService: ClientService;

  // LOGGER
  logger: LoggerInstance;
  loggerFactory: LoggerFactoryService;

  // HEALTH
  healthService: HealthService;

  // JOBS
  // jobService: JobService;

  // DASH
  dashboardService: DashboardService;

  // BOOTSTRAP
  loggerBootstrap: LoggerBootstrap;
  expressBootstrap: ExpressBootstrap;
  mongoBootstrap: MongoBootstrap;
  routerBootstrap: RouterBootstrap;
  strategyBootstrap: StrategyBootstrap;
  jobBootstrap: JobBootstrap;

  // CORE
  private _maintenance = false;
  private _killInitiated = false;
  private teardownFunctions: (() => void | Promise<void>)[] = [];
  private teardownPromise: Promise<void>;
  private gracefulExitEnabled: boolean;

  beforeExit = () => {};

  set maintenance(value: boolean) {
    this._maintenance = value;
  }

  get maintenance(): boolean {
    return this._maintenance;
  }

  constructor(public readonly env: AppEnvironment, public readonly config: AppConfig = getDefaultAppConfig()) {
    // First set up bootstrap protocol
    this.loggerBootstrap = new LoggerBootstrap();
    this.expressBootstrap = new ExpressBootstrap();
    this.mongoBootstrap = new MongoBootstrap();
    this.strategyBootstrap = new StrategyBootstrap();
    this.routerBootstrap = new RouterBootstrap();
    this.jobBootstrap = new JobBootstrap();

    // Dash (special mid)
    this.dashboardService = new DashboardService(this);

    // Register req pre-process and post-process
    this.routerBootstrap.registerBeforeInit(RouteHandler.registerPreProcessor);
    this.routerBootstrap.registerBeforeInit(RouteHandler.registerRequestPrepareHandler);
    this.routerBootstrap.registerBeforeInit(RouteHandler.registerLoggerHandler);
    this.routerBootstrap.registerAfterInit(RouteHandler.registerErrorHandler);
    this.routerBootstrap.registerAfterInit(RouteHandler.registerRedirect);

    // Other services and core API
    this.healthService = new HealthService(this);
    this.routerBootstrap.registerAfterInit(SwaggerService.serve);

    this.printAppInfo();
  }

  print(msg) {
    if (!this.config.silentInit) {
      if (this.env.onlyJsonLogs) {
        // kubernetes readable format
        console.info(JSON.stringify({ message: msg }));
      } else {
        console.info(msg);
      }
    }
  }

  printInline(msg) {
    if (!this.config.silentInit) {
      process.stdout.write(msg);
    }
  }

  printAppInfo() {
    if (this.env.onlyJsonLogs) {
      // kubernetes readable format
      console.log(JSON.stringify({
        message: 'app init done',
        type   : this.env.type,
        mode   : this.env.mode,
        id     : this.env.id,
        name   : this.env.name,
        version: this.env.version,
      }));
    } else {
      this.print(`╒═════════════════════════`);
      this.print(`│ App initialization done.`);
      this.print(`│ `);
      this.print(`│ Environment:  ${ this.env.type }`);
      this.print(`│ Mode:         ${ this.env.mode }`);
      this.print(`│ `);
      this.print(`│ App id:       ${ this.env.id }`);
      this.print(`│ App name:     ${ this.env.name }`);
      this.print(`│ App version:  ${ this.env.version }`);
      this.print(`╘═════════════════════════`);
    }
  }

  private printServerInfo() {
    if (this.env.onlyJsonLogs) {
      // kubernetes readable format
      console.log(JSON.stringify({
        message   : 'server is up',
        apiBaseUrl: this.env.apiBaseUrl
      }));
    } else {
      this.print(`╒═════════════════════════`);
      this.print(`│ Server is UP`);
      this.print(`│ `);
      this.print(`│ Api url: ${ this.env.apiBaseUrl }`);
      if (SwaggerService.enabled(this)) {
        this.print(`│ Api specification: ${ this.env.apiBaseUrl }/_/api`);
      }
      this.print(`╘═════════════════════════`);
    }
  }

  /**
   * Initialize all core application services.
   * @returns {Promise<this>}
   */
  init() {
    return this.bootstrap();
  }

  private async bootstrapService(name: string, service: Bootstrap) {
    if (service.enabled(this)) {
      this.print(`│ Bootstrapping ${ name } ...`);
      await service.startInit(this);
    }
  }

  private async bootstrap() {
    this.print(`│ App bootstrap started`);
    const startTime = Date.now();

    await this.bootstrapService('logger', this.loggerBootstrap);
    await this.bootstrapService('mongo', this.mongoBootstrap);
    await this.bootstrapService('strategy', this.strategyBootstrap);
    await this.bootstrapService('express', this.expressBootstrap);
    await this.bootstrapService('router', this.routerBootstrap);
    await this.bootstrapService('jobs', this.jobBootstrap);

    this.print(`│ App bootstrap finished in ${ (Date.now() - startTime) / 1000 }s`);
    return this;
  }

  /**
   * Initialize app and start HTTP server.
   * @returns {this}
   */
  serve() {
    (async () => {
      await this.init();
      await this.listen();
    })()
      .catch(e => {
        console.error(e);
        process.abort();
      });
    return this;
  }

  /**
   * Start HTTP server.
   * @returns {Promise<void>}
   */
  async listen() {
    this.enableGracefulExit();

    this.expressApp.set('port', this.config.express.serverPort || 80);

    await (new Promise((resolve, reject) => {
      try {
        this.expressServer = this.expressApp.listen(this.expressApp.get('port'), resolve);
      } catch (e) {
        reject(e);
      }
    }));

    this.printServerInfo();
  }

  lambdaReady() {
    this.enableGracefulExit();
    this.printServerInfo();
  }

  /**
   * Register function to be called when application is shutting down.
   * @param {() => Promise<void>} fun
   */
  addTeardownFunction(fun: () => Promise<void>) {
    this.teardownFunctions.push(fun);
  }

  /**
   * Destroy application.
   * @returns {Promise<void>}
   */
  teardown(): Promise<void> {
    if (this.teardownPromise) {
      return this.teardownPromise;
    }

    this.teardownPromise = (async () => {
      this.maintenance = true;
      const errors = [];
      try {
        await Promise.race([this.expressBootstrap.teardown(this).catch(e => errors.push(e)), sleep(150)]);

        await this.jobBootstrap.teardown(this).catch(e => errors.push(e));

        for (const tf of this.teardownFunctions) {
          try {
            await tf();
          } catch (e) {
            errors.push(e);
          }
        }

        // ISSUE https://github.com/Automattic/mongoose/issues/4507
        // await this.mongoBootstrap.teardown(this).catch(e => errors.push(e));

        await this.loggerBootstrap.teardown(this).catch(e => errors.push(e));

        await this.beforeExit();
      } catch (e) {
        errors.push(e);
        throw errors;
      }
      if (errors.length) {
        throw errors;
      }
    })();

    return this.teardownPromise;
  }

  exit(code?: number) {
    if (!this._killInitiated) {
      this._killInitiated = true;
      console.info('Exiting app.');
      this.teardown()
        .then(() => {
          console.info('All services stopped, exiting.');
          process.exit(code !== undefined ? code : 0)
        })
        .catch(e => {
          console.info(`App didn't exit cleanly.`);
          console.error(e);
          process.exit(code !== undefined && code > 0 ? code : 1)
        })
    } else {
      console.info('Exit already triggered.');
    }
  }

  /**
   * Listen for SIGTERM and teardown application.
   */
  enableGracefulExit() {
    if (!this.gracefulExitEnabled) {
      process.on('SIGINT', () => {
        console.info('Received SIGINT.');
        this.exit();
      });
      process.on('SIGTERM', () => {
        console.info('Received SIGTERM.');
        this.exit();
      });
      process.on('exit', () => {
        console.info('Received exit.');
        this.exit();
      });
    }
  }
}
