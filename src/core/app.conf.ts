import { SwaggerConfig }                                        from '../common/services/swagger/swagger.conf';
import { LoggerConfig }                                         from '../common/services/logger/logger.conf';
import { ExpressConfig, getDefaultExpressConfig }               from '../common/services/express/express.conf';
import { getDefaultLoggerConfig }                               from '../common/services/logger';
import { getDefaultSwaggerConfig }                              from '../common/services/swagger';
import { AuthenticationConfig, getDefaultAuthenticationConfig } from '../common/services/authentication/authentication.conf';
import { MongoConfig }                                          from '../common/services/mongo';

export interface AppConfig {
  silentInit?: boolean;

  express?: ExpressConfig
  mongo?: MongoConfig
  swagger?: SwaggerConfig
  logger?: LoggerConfig
  authentication?: AuthenticationConfig;
}

export const getDefaultAppConfig = (): AppConfig => ({
  authentication: getDefaultAuthenticationConfig(),
  express       : getDefaultExpressConfig(),
  swagger       : getDefaultSwaggerConfig(),
  logger        : getDefaultLoggerConfig()
});
