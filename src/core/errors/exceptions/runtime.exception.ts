export declare class Error {
  public name: string;
  public message: string;
  public stack: string;

  constructor(message?: string);
}

export class RuntimeException extends Error {

  data: any;

  constructor(private msg = ``, data?: any) {
    super(msg);
    this.data = data;
  }

  public what() {
    return this.msg;
  }
}
