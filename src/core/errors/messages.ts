export const INVALID_EXCEPTION_FILTER = `Invalid exception filter`;
export const UNKNOWN_EXCEPTION_MESSAGE = `Internal server error`;
