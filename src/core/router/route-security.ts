import { TsoaRoute }          from '@kalmia/tsoa';
import { App }                from '../app';
import * as express           from 'express';
import { BaseAppRequest }     from '../../common/interfaces';
import { BaseAuthAppRequest } from '../../common/interfaces/request.interface';

export class RouteSecurity {
  static getAuthenticateMiddleware(app: App, security: TsoaRoute.Security[] = []) {
    return async (request: BaseAppRequest, response: express.Response, next: express.NextFunction) => {
      try {
        await app.connector.connectRequest(request, security);
        await app.authentication.authenticateRequest(request, security);

        // --- from here on is BaseAuthAppRequest ---

        await app.authorization.authorizeRequest(request as BaseAuthAppRequest, security);
        await app.accounting.accountRequest(request as BaseAuthAppRequest, security);

        next();
      } catch (e) {
        next(e)
      }
    }
  }
}
