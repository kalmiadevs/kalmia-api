import { Controller }                         from '@kalmia/tsoa';
import { App }                                from '../app';
import { ErrorHandler }                       from './middlewares/error-handler';
import { BaseAppRequest, BaseAuthAppRequest } from '../../common/interfaces/request.interface';
import * as express                           from 'express';
import { HttpException }                      from '../../common/exceptions';
import { isString }                           from 'lodash';
import * as bodyParser                        from 'body-parser'
import * as methodOverride                    from 'method-override';
import { getRequestPrepareHandler }           from './middlewares/req-handler';
import * as Raven                             from 'raven';
import * as cookieParser                      from 'cookie-parser';
import * as formData                          from 'express-form-data';
import { base64Decode }                       from '../../libs';

export class RouteHandler {
  static registerPreProcessor(app: App) {
    // before all other middleware/route handlers
    if (app.loggerFactory.raven) {
      app.expressApp.use(Raven.requestHandler());
    }

    app.expressApp.use(bodyParser.urlencoded({ extended: true, limit: '25mb' }));
    app.expressApp.use(bodyParser.json({
      limit : '50mb',
      verify: function (req, res, buf, encoding) {
        req['rawBuffer'] = buf;
        req['rawEncoding'] = encoding;
      }
    }));
    app.expressApp.use(formData.parse({ autoclean: true }));
    app.expressApp.use(methodOverride());
    app.expressApp.use(cookieParser());
  }

  static handlePromise(controllerObj: any | null, promise: any, request: BaseAppRequest, response: express.Response, next: any) {
    return Promise.resolve(promise)
      .then((data: any) => {
        if (request.ctx._preventDefaultResponse) {
          return;
        }
        if (data instanceof HttpException) {
          next(data);
        }
        let statusCode;
        if (controllerObj !== null && controllerObj instanceof Controller) {

          const controller = controllerObj as Controller;
          const headers = controller.getHeaders();
          Object.keys(headers).forEach((name: string) => {
            response.set(name, headers[name]);
          });

          statusCode = controller.getStatus();
        }

        if (data) {
          response.status(statusCode || 200).json(data);
        } else {
          response.status(statusCode || 204).end();
        }
      })
      .catch((error: any) => next(error));
  }

  static registerErrorHandler(app: App) {
    // before all other error handlers
    if (app.loggerFactory.raven) {
      app.expressApp.use(Raven.errorHandler());
    }

    app.errorHandler = new ErrorHandler(app);
    app.expressApp.use((exception: Error | HttpException | any,
                        request: BaseAuthAppRequest,
                        response: express.Response,
                        next: express.NextFunction) => {
      app.errorHandler.handle(exception, request, response, next)
    });
  }

  static registerRedirect(app: App) {
    app.expressApp.all(['/_/redirect/base/:base64where', '/api/_/redirect/base/:base64where'], (req, res) => {
      const instructions: {
        query: any;
        path: string;
        client: string;
      } = JSON.parse(base64Decode(req.params.base64where));

      if (!isString(instructions.path)) {
        throw new Error('Path must be a string!');
      }

      req.url = instructions.path;
      req.headers['x-client-key'] = instructions.client;

      (req.app as any).handle(req, res);
    });
  }

  static registerRequestPrepareHandler(app: App) {
    app.expressApp.use(getRequestPrepareHandler(app));
  }

  static registerLoggerHandler(app: App) {
    if (!app.env.isQA) {
      app.expressApp.use(app.loggerFactory.createExpressMiddleware());
    }
  }
}
