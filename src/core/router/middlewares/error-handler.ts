import * as express                           from 'express';
import { BaseAppRequest, BaseAuthAppRequest } from '../../../common/interfaces/request.interface';
import { ExceptionFilterMetadata }            from '../../../common/interfaces/exceptions';
import { isEmpty, isObject }                  from '../../../common/utils';
import { HttpException }                      from '../../../common/exceptions';
import { UNKNOWN_EXCEPTION_MESSAGE }          from '../../errors/messages';
import * as uuidv1                            from 'uuid/v1';
import { ExceptionFilter }                    from '../../../common/interfaces/exceptions/exception-filter.interface';
import { Metatype }                           from '../../../common/interfaces/metatype.interface';
import { App }                                from '../../app';

export class ErrorHandler {

  private readonly logger = console;

  private filters: ExceptionFilterMetadata[] = [];

  constructor(private app: App) {}

  handle(exception: Error | HttpException | any,
         request: BaseAuthAppRequest | BaseAppRequest,
         response: express.Response,
         next?: express.NextFunction) {

    if (this.invokeCustomFilters(exception, response)) { return; }

    if (!(exception instanceof HttpException)) {
      request.ctx = request.ctx || {} as any;
      request.ctx.error = exception;

      const errorId = uuidv1();
      const resMsg: any = {
        statusCode: 500,
        message   : UNKNOWN_EXCEPTION_MESSAGE,
        errorId
      };

      if (isObject(exception) && (exception as Error).message) {
        this.logger.error(
          (exception as Error).message,
          (exception as Error).stack,
        );
        if (this.app.env.isDevOrQA) {
          resMsg.errorMessage = (exception as Error).message;
          resMsg.errorStack = (exception as Error).stack;
        }
      } else {
        this.logger.error('Unknown exception', { ...exception, errorId });
        resMsg.exception = exception;
      }

      response.status(500).json(resMsg);
      return;
    }

    const res = exception.getResponse();
    const message = isObject(res)
                    ? res
                    : {
        statusCode: exception.getStatus(),
        message   : res,
      };
    response.status(exception.getStatus()).json(message);
  }

  public setCustomFilters(filters: ExceptionFilterMetadata[]) {
    this.filters = filters;
  }

  public addCustomFilter(handler: ExceptionFilter['catch'], filter?: Metatype<any>[]) {
    this.filters = this.filters || [];
    this.filters.push({ func: handler, exceptionMetaTypes: filter });
  }

  public invokeCustomFilters(exception, response): boolean {
    if (isEmpty(this.filters)) {
      return false
    }

    const filter = this.filters.find(({ exceptionMetaTypes, func }) => {
      const hasMetaType =
              !exceptionMetaTypes ||
              !!exceptionMetaTypes.find(
                ExceptionMetatype => exception instanceof ExceptionMetatype,
              );
      return hasMetaType;
    });

    return !!filter && !!filter.func(exception, response);
  }
}
