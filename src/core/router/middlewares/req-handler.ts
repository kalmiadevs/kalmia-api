import { BaseAppRequest } from '../../../common/interfaces/request.interface';
import * as uuidv1        from 'uuid/v1';
import { App }            from '../../app';

export function getRequestPrepareHandler(app: App) {
  return (req: BaseAppRequest, res, next) => {
    req.ctx = req.ctx || {} as any;
    req.ctx.id = uuidv1();

    req.preventDefaultResponse = (val?: boolean) => {
      req.ctx._preventDefaultResponse = val !== undefined ? val : true;
    };

    res.set({
      'Cache-Control': 'no-cache',
      'Pragma': 'no-cache',
      'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT',
      'If-Modified-Since': '0'
    });

    if (app.maintenance) {
      res.send(503).json({
        message: 'API is currently undergoing maintenance.'
      });
    } else {
      next();
    }
  };
}
