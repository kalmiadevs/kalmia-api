import errorHandler from './error-handler';
import mockReq = require('../../../../test/res/jest-mock-express/request');
import mockRes = require('../../../../test/res/jest-mock-express/response');

describe('Middleware error handler.', () => {
  it('should return 500', (done) => {
    const req = mockReq();
    const res = mockRes();

    errorHandler(Error(), req as any, res, () => {
      done();
    });

    expect(res.status).toHaveBeenCalledWith(500);
    done();
  });
});
