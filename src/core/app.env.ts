import { ApiEnvironment } from '../common/enums/api-environment.enum';
import { ApiMode }        from '../common/enums/api-mode.enum';
import * as os            from 'os';
import * as shortid       from 'shortid';

export abstract class AppEnvironment {

  key: string = process.env.ENV_KEY || 'unknown';

  shortId = shortid.generate();

  id = `${ os.hostname().toLowerCase().replace(/[^a-zA-Z0-9]+/g, '') }-${ this.shortId }`;

  location: {
    type: 'aws' | 'do' | 'on-premise' | 'unknown',
    cloudArchitecture: 'docker-swarm' | 'serverless' | 'unknown',
  } = {
    type: process.env.ENV_LOCATION as any || 'unknown',
    cloudArchitecture: process.env.ENV_CLOUD_ARHITECTURE as any || 'unknown'
  };

  /**
   * Set from package.json
   */
  name = `unknown_api`;

  /**
   * Set from package.json
   */
  version = `0.0.0`;

  build = {
    date  : process.env.BUILD_DATE || 'unknown',
    code  : process.env.BUILD_CODE || 'unknown',
    commit: process.env.SOURCE_COMMIT || 'unknown',
    branch: process.env.SOURCE_BRANCH || 'unknown'
  };

  /**
   * Is SERVERLESS.
   */
  isServerless = process.env.ENV_TYPE === 'SERVERLESS';

  /**
   * API repository root path
   */
  rootPath = process.cwd();

  abstract type: ApiEnvironment;

  abstract mode: ApiMode;

  /**
   * API base url without version postfix
   */
  abstract apiBaseUrl: string;

  get apiHost(): string {
    return this.apiBaseUrl.split('//')[0];
  }

  get isDevOrQA(): boolean {
    return this.isDev || this.isQA;
  }

  get isDev(): boolean {
    return this.type === ApiEnvironment.Dev;
  }

  get isQA(): boolean {
    return this.type === ApiEnvironment.QA;
  }

  get isProd(): boolean {
    return this.type === ApiEnvironment.Prod;
  }

  get onlyJsonLogs(): boolean {
    return this.isProd;
  }

  get isWorker(): boolean {
    return this.mode === ApiMode.Worker;
  }

  get isServer(): boolean {
    return this.mode === ApiMode.Server;
  }

  get isTest(): boolean {
    return this.mode === ApiMode.Test;
  }
}
