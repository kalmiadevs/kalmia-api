import { App }                                                  from './app';
import { AppConfig, getDefaultAppConfig }                       from './app.conf';
import { SwaggerConfig }                                        from '../common/services/swagger/swagger.conf';
import { LoggerConfig }                                         from '../common/services/logger/logger.conf';
import { AppEnvironment }                                       from './app.env';
import { Container }                                            from 'typedi';
import { getDefaultLoggerConfig }                               from '../common/services/logger';
import { getDefaultSwaggerConfig }                              from '../common/services/swagger';
import { ExpressConfig, getDefaultExpressConfig }               from '../common/services/express';
import { AuthenticationConfig, getDefaultAuthenticationConfig } from '../common/services/authentication';
import { ApiEnvironment, ApiMode }                              from '../common/enums';
import { MongoConfig }                                          from '../common/services/mongo';

class Environment extends AppEnvironment {
  mode = ApiMode.Test;
  type = ApiEnvironment.QA;
  rootPath = process.cwd();
  apiBaseUrl = 'localhost';
}

export class AppBuilder {
  conf: AppConfig;
  env: AppEnvironment;

  static async initMinQA() {
    const env = new Environment();
    env.type = ApiEnvironment.QA;
    const app = AppBuilder.create(env)
      .setSilentInit()
      .build();
    await app.loggerBootstrap.init(app);
    return app;
  }

  public static create(env: AppEnvironment) {
    const x = new AppBuilder();
    x.conf = {
      authentication: getDefaultAuthenticationConfig()
    };
    x.env = env;
    return x;
  }

  setConfig(conf: AppConfig) {
    this.conf = conf;
    return this;
  }

  setDefaultConfig(conf: AppConfig) {
    this.conf = getDefaultAppConfig();
    return this;
  }

  setSilentInit() {
    this.conf.silentInit = true;
    return this;
  }

  setAuthenticationConfig(authentication: AuthenticationConfig = getDefaultAuthenticationConfig()) {
    this.conf.authentication = authentication;
    return this;
  }

  withSwagger(swagger: SwaggerConfig = getDefaultSwaggerConfig()) {
    this.conf.swagger = swagger;
    return this;
  }

  withLogger(logger: LoggerConfig = getDefaultLoggerConfig()) {
    this.conf.logger = logger;
    return this;
  }

  withMongo(mongoose: MongoConfig) {
    this.conf.mongo = mongoose;
    return this;
  }

  withExpress(express: ExpressConfig = getDefaultExpressConfig()) {
    this.conf.express = express;
    return this;
  }

  updateConf(cb: (x: AppConfig) => void) {
    cb(this.conf);
    return this;
  }

  build() {
    const app = new App(this.env, this.conf);
    Container.set(App, app);
    return app;
  }
}
