import { App }                  from '../app';
import { Bootstrap }            from './bootstrap';
import { LoggerFactoryService } from '../../common/services/logger/logger.service';
import * as Raven               from 'raven';

export class LoggerBootstrap extends Bootstrap {
  async init(app: App) {
    let raven;
    if (app.env.isProd && app.config.logger.sentryDns) {
      raven = Raven.config(app.config.logger.sentryDns, {
        captureUnhandledRejections: true,
        release                   : app.env.version,
        environment               : app.env.type,
        tags                      : {
          id  : app.env.id,
          mode: app.env.mode,
        }
      }).install();
    }

    app.loggerFactory = new LoggerFactoryService(app, { raven });
    app.logger = app.loggerFactory.create('app', ['core']);
  }
}
