import { App } from '../app';

export abstract class Bootstrap {
  beforeInitialize: ((app: App) => void | Promise<void>)[] = [];
  afterInitialize: ((app: App) => void | Promise<void>)[] = [];

  enabled(app: App): boolean {
    return true;
  }

  registerBeforeInit(fun: (app: App) => void | Promise<void>) {
    this.beforeInitialize.push(fun);
  }

  registerAfterInit(fun: (app: App) => void | Promise<void>) {
    this.afterInitialize.push(fun);
  }

  async startInit(app: App) {
    for (const x of this.beforeInitialize) {
      await x(app);
    }

    await this.init(app);

    for (const x of this.afterInitialize) {
      await x(app);
    }
  }

  abstract async init(app: App);

  async teardown(app: App): Promise<any> {};
}

export interface BootstrapConstructor<T> {
  new(): T;
}
