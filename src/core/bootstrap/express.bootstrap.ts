import { App }       from '../app';
import * as express  from 'express';
import * as helmet   from 'helmet';
import { Bootstrap } from './bootstrap';
import * as cors     from 'cors';

export class ExpressBootstrap extends Bootstrap {
  enabled(app: App) {
    return !!app.config.express;
  }

  async init(app: App) {
    app.expressApp = app.config.express.expressInstance || express();
    app.expressApp.use(helmet());
    app.expressApp.disable('x-powered-by');

    const corsOptions: cors.CorsOptions = {
      allowedHeaders   : app.config.express.allowedHeaders,
      credentials      : true,
      methods          : 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
      origin           : true,
      preflightContinue: false
    };
    app.expressApp.use(cors(corsOptions));
  }

  async teardown(app: App) {
    if (app.expressServer) {
      return new Promise((resolve, reject) => {
        app.expressServer.close(resolve);
      });
    }
  }
}
