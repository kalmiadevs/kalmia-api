import { App }           from '../app';
import { Bootstrap }     from './bootstrap';
import { MongoService }  from '../../common/services/mongo';
import { ClientService } from '../../common/services/client/client.service';

export class MongoBootstrap extends Bootstrap {
  enabled(app: App) {
    return !!app.config.mongo;
  }

  async init(app: App) {
    const { connectionString, connectionStringLog } = MongoService.prepareConnectionString(app.config.mongo);
    const mongooseOptions = app.config.mongo.mongooseOptions || {};
    app.print(`│  Connecting to ${ connectionStringLog }`);

    app.mongoService = new MongoService(app);
    await app.mongoService.connect(connectionString, mongooseOptions, app.config.mongo);

    app.clientService = new ClientService(app);
    app.mainDb = app.mongoService.getDb('main');
  }

  async teardown(app: App) {
    if (!app.config.mongo) {
      return;
    }

    console.info(`Closing mongo connection ...`);
    await app.mongoService.disconnect();
    console.info(`Mongo connection closed.`);
  }
}
