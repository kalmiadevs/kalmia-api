import { App }                   from '../app';
import { Bootstrap }             from './bootstrap';
import { AccountingService }     from '../../common/services/accounting/accounting.service';
import { AuthenticationService } from '../../common/services/authentication';
import { AuthorizationService }  from '../../common/services/authorization';
import { ConnectorService }      from '../../common/services/connector';

export class StrategyBootstrap extends Bootstrap {
  async init(app: App) {
    app.accounting = new AccountingService(app);
    app.authentication = new AuthenticationService(app);
    app.authorization = new AuthorizationService(app);
    app.connector = new ConnectorService(app);
  }
}
