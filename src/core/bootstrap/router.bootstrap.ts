import { App }       from '../app';
import { Bootstrap } from './bootstrap';

export class RouterBootstrap extends Bootstrap {
  enabled(app: App) {
    return !!app.config.express;
  }

  async init(app: App) {}
}
