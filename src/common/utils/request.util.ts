import { BaseAppRequest, BaseAuthAppRequest } from '../interfaces/request.interface';

export function isAuthAppRequest(req: any): req is BaseAuthAppRequest {
  return !!(req.ctx && req.ctx.token)
}

export function isAppRequest(req: any): req is BaseAppRequest {
  return !!req.id
}
