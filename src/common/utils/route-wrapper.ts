import * as express       from 'express';
import { RouteHandler }   from '../../core/router';
import { BaseAppRequest } from '../interfaces/request.interface';

/**
 * Allows for easy async route with error handler (just throw exception).
 *
 * Example:
 *  router.post('/', routeWrapper(async (req: Request, res: Response) => {
 *   ...
 *   return { bar: 'foo', ... }
 *  }));
 *
 * @param {(req: Request, res: Response) => Promise<any>} fun
 * @returns {(req: Request, res: Response) => void}
 */
export function routeWrapper(fun: (request: BaseAppRequest, response: express.Response) => Promise<any>) {
  return (request: BaseAppRequest, response: express.Response, next: any) => {
    RouteHandler.handlePromise(null, fun(request, response), request, response, next).catch(e => console.error(e));
  };
}
