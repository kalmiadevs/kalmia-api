import { ObjectID } from 'mongodb';

export function toObjectID(id: string | ObjectID): ObjectID {
  if (typeof id === 'string') {
    return new ObjectID(id);
  }
  return id;
}
