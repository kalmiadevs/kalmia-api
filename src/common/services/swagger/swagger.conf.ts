export interface SwaggerConfig {
  allowOnProd?: boolean;
  swaggerFile?: string;
}

export const getDefaultSwaggerConfig = (): SwaggerConfig => ({
  swaggerFile: `${ process.cwd() }/res/swagger.json`
});
