import { App }        from '../../../core';
import * as swaggerUi from 'swagger-ui-express';
import * as express   from 'express';

export class SwaggerService {
  static async enabled(app: App) {
    if (!app.config.swagger || (!app.config.swagger.allowOnProd && app.env.isProd) || app.env.isQA) {
      return false;
    }
    return true;
  }

  static async serve(app: App) {
    if (app.config.swagger) {
      if ((!app.config.swagger.allowOnProd && app.env.isProd) || app.env.isQA) {
        return;
      }
      const options = {
        swaggerOptions: {
          validatorUrl: null
        }
      };
      const swaggerDocument = require(app.config.swagger.swaggerFile || `${ process.cwd() }/swagger.json`);
      swaggerDocument.host = app.env.apiHost;

      app.expressApp.use('/_/api', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));

      app.expressApp.get('/_/api.json', (req: any, res: express.Response) => {
        const doc = require(app.config.swagger.swaggerFile || `${ process.cwd() }/swagger.json`);
        doc.host = app.env.apiHost;
        res.json(doc);
      });
    }
  }
}
