import { App }                                                 from '../../../core/index';
import { InternalServerErrorException, UnauthorizedException } from '../../exceptions/index';
import { AuthenticationStrategy }                              from './authentication.strategy';
import { TsoaRoute }                                           from '@kalmia/tsoa';
import * as bcrypt                                             from 'bcryptjs';
import { BaseAppRequest }                                      from '../../interfaces/index';

export class AuthenticationService {

  private strategies: { [key: string]: AuthenticationStrategy } = {};

  get saltRounds() {
    return this.app.config.authentication.saltRounds;
  }

  constructor(private app: App) {}

  addStrategy(name: string, strategy: AuthenticationStrategy) {
    this.strategies[name] = strategy;
  }

  addNoOpStrategy(name: string) {
    this.strategies[name] = null;
  }

  async authenticateRequest(request: BaseAppRequest, security: TsoaRoute.Security[] = []) {
    for (const secMethod of security) {
      if (this.strategies[secMethod.name]) {
        if (!await this.strategies[secMethod.name].authenticateRequest(request, secMethod.scopes, security)) {
          throw new UnauthorizedException();
        }
      } else if (this.strategies[secMethod.name] === null) {
        // noop
      } else {
        throw new InternalServerErrorException('Unknown authentication strategy');
      }
    }
  }

  async hashPassword(password: string) {
    const salt = await bcrypt.genSalt(this.saltRounds);
    return bcrypt.hash(password, salt);
  }

  comparePassword(plaintextPassword: string, hash: string) {
    return bcrypt.compare(plaintextPassword, hash);
  }
}
