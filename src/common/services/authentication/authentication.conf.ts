export interface AuthenticationConfig {
  saltRounds: number;
}

export const getDefaultAuthenticationConfig = (): AuthenticationConfig => ({
  saltRounds: 10
});
