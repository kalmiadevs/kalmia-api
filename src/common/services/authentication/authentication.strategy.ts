import { TsoaRoute }      from '@kalmia/tsoa';
import { BaseAppRequest } from '../../interfaces/index';

/**
 * Authentication strategy.
 */
export abstract class AuthenticationStrategy {

  /**
   * Authenticate express request. If authenticated return true else false or throw exception..
   * Sets metadata to request.ctx (BaseAppRequest => BaseAuthAppRequest).
   *
   * @param {Request} request
   * @param {string[]} scopes
   * @param {TsoaRoute.Security[]} security
   * @returns {Promise<boolean>}
   */
  abstract authenticateRequest(request: BaseAppRequest, scopes: string[], security: TsoaRoute.Security[]): Promise<boolean>;
}
