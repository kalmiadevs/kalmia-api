import { Express } from 'express';

export interface ExpressConfig {
  allowedHeaders: string[];
  expressInstance?: Express;
  serverPort?: number;
}

export const getDefaultExpressConfig = (): ExpressConfig => ({
  allowedHeaders: [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'X-Access-Token',
    'ngsw-bypass',

    /**
     * Used for secure forms for public access
     */
    'X-Secure-Access-Token',
    'X-Client-Key',
    'TimeZone',
    'Authorization',
    'Cache-Control',
    'Pragma',
    'Expires',
    'If-Modified-Since'
  ],
});
