export interface MongoConfig {
  mongoHost?: string;
  mongoDatabase?: string;
  mongoUser?: string;
  mongoPass?: string;
  mongooseOptions?: any;

  /**
   * If set other opt are ignored.
   */
  mongoConnectionString?: string;

  dbPrefix?: string;
}
