import { MongoClient, MongoClientOptions, MongoError } from 'mongodb';
import { MongoConfig, MongoConnectionString }          from '.';
import { App }                                         from '../../../core';

export class MongoService {

  prefix = 'v1_';

  client: MongoClient;

  /**
   * Set at start.
   */
  mongoConfig: MongoConfig;

  static prepareConnectionString(mongoConfig: MongoConfig): MongoConnectionString {
    if (mongoConfig.mongoConnectionString) {
      return {
        connectionString   : mongoConfig.mongoConnectionString,
        connectionStringLog: mongoConfig.mongoConnectionString.substring(0, 8) + '****' + mongoConfig.mongoConnectionString.slice(-8)
      }
    }

    let mongoHost = mongoConfig.mongoHost;
    let mongoHostLog = mongoConfig.mongoHost;

    if (mongoConfig.mongoUser) {
      mongoHost = `${ mongoConfig.mongoUser }${ mongoConfig.mongoPass ? ':' : '' }${ mongoConfig.mongoPass }@${ mongoHost }`;
      mongoHostLog = `${ mongoConfig.mongoUser }:*********@${ mongoHost }`;
    }

    return {
      connectionString   : `mongodb://${ mongoHost }`,
      connectionStringLog: `mongodb://${ mongoHostLog }`
    }
  }

  constructor(private app: App) {}

  async connect(uri: string, mongoClientOptions: MongoClientOptions, mongoConfig: MongoConfig) {
    // used for backups in main app
    this.mongoConfig = mongoConfig;

    this.prefix = mongoConfig.dbPrefix || this.prefix;

    const concatMongoOptions: MongoClientOptions = {
      appname: this.app.env.id,
      useNewUrlParser: true,
      ...mongoClientOptions
    };

    this.client = await (new Promise<MongoClient>((resolve, reject) => {
      MongoClient.connect(uri, concatMongoOptions, (err: MongoError, client: MongoClient) => {
        if (err) {
          reject(err);
        } else {
          resolve(client);
        }
      });
    }));
  }

  /**
   * Returns with prefix v1_
   * @param name
   */
  getDb(name: string) {
    return this.client.db(this.getDbName(name));
  }

  getDbName(name: string) {
    return this.prefix + name;
  }

  disconnect() {
    return this.client.close();
  }

  isConnected() {
    return this.client.isConnected();
  }
}
