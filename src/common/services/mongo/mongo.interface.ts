export interface MongoConnectionString {
  connectionString: string;
  connectionStringLog: string;
}
