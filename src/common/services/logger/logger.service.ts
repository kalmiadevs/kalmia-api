import * as winston                          from 'winston';
import { LoggerInstance, TransportInstance } from 'winston';
import { App }                               from '../../../core/app';
import * as expressWinston                   from 'express-winston';
import { LoggerConfig }                      from './logger.conf';
import { Handler }                           from 'express';
import { isAppRequest, isAuthAppRequest }    from '../../utils';
import { Client as RavenClient }             from 'raven';
import { Sentry }                            from './winston-raven-sentry';

export class LoggerFactoryService {

  static readonly levelsConf = {
    levels: {
      emerg  : 0,
      alert  : 1,
      crit   : 2,
      error  : 3,
      warn   : 4,
      notice : 5,
      info   : 6,
      http   : 7,
      verbose: 8,
      debug  : 9,
      silly  : 10
    },
    colors: {
      error  : 'red',
      warn   : 'yellow',
      info   : 'green',
      http   : 'green',
      verbose: 'cyan',
      debug  : 'blue',
      silly  : 'magenta',
      emerg  : 'red',
      alert  : 'yellow',
      crit   : 'red',
      warning: 'red',
      notice : 'yellow'
    }
  };

  conf: LoggerConfig;
  raven?: RavenClient;
  sentry?: TransportInstance;
  isWorker?: boolean;

  disabled: boolean;

  cache: { [category: string]: LoggerInstance } = {};

  constructor(private app: App, options: {
    raven?: RavenClient
  }) {
    this.isWorker = app.env.isWorker;

    this.conf = app.config.logger;
    if (!this.conf) {
      this.disabled = true;
      return;
    }

    this.conf.consoleLevel = this.conf.consoleLevel || 'emerg';

    //
    // Make winston aware of these colors
    //
    winston.addColors(LoggerFactoryService.levelsConf.colors);

    this.raven = options.raven;
    if (this.raven) {
      this.sentry = new Sentry({
        raven    : this.raven,
        level    : 'notice',
        levelsMap: {
          emerg : 'error',
          alert : 'error',
          crit  : 'error',
          error : 'error',
          warn  : 'warning',
          notice: 'info',

          // silly  : 'debug',
          // verbose: 'debug',
          // info   : 'info',
          // debug  : 'debug'
        }
      })
    }
  }

  createExpressMiddleware(): Handler {
    const transports: TransportInstance[] = this.conf.enableConsoleHttp ? [
      new (winston.transports.Console)({
        json: true,
        stringify: (obj) => {
          obj._tags = 'access';
          return JSON.stringify(obj);
        },
      })
    ] : [];

    return expressWinston.logger({
      transports,
      ...{ statusLevels: true } as any,
      expressFormat   : true,
      colorize        : true,
      requestWhitelist: ['url', 'headers', 'method', 'httpVersion', 'originalUrl', 'query', 'body'],
      meta            : true,
      dynamicMeta     : (req: any, res) => {
        const obj: any = {};
        if (isAppRequest(req)) {
          if (req.ctx.error) {
            if (req.ctx.error.message) {
              obj.error = {
                message: req.ctx.error.message,
                stack  : req.ctx.error.stack
              };
            } else {
              obj.error = req.ctx.error;
            }
          }
          if (isAuthAppRequest(req)) {
            obj.user = (req.ctx.token as any).uid;
          }
        }
        return obj;
      },
      ignoreRoute     : (req, res) => {
        return false;
      },
      ignoredRoutes   : ['/_/health']
    })
  }

  create(category: string, tags: string[] = []): LoggerInstance {
    if (!this.cache[category]) {
      this.cache[category] = this._create(category, tags);

      this.cache[category].log = function () {
        const args = arguments;
        args[2] = args[2] || {};

        if (!args[2]._tags) {
          args[2]._tags = [];
        }

        args[2]._tags = args[2]._tags.concat(tags);

        winston.Logger.prototype.log.apply(this, args);
      } as any;
    }

    return this.cache[category];
  }

  private _create(category: string, tags: string[]): LoggerInstance {
    if (this.disabled) {
      return new winston.Logger({ transports: [] });
    }

    const transports = [
      this.createConsoleTransport(),
      ...this.getSentry()
    ];

    return new winston.Logger({
      transports,
      levels: LoggerFactoryService.levelsConf.levels
    });
  }

  private createConsoleTransport(): TransportInstance {
    return new (winston.transports.Console)({
      level      : this.conf.consoleLevel,
      json: true,
      stringify: (obj) => JSON.stringify(obj),
    })
  }

  private getSentry(): TransportInstance[] {
    return this.app.loggerFactory.sentry ? [this.app.loggerFactory.sentry] : [];
  }
}
