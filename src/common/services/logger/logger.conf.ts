export interface LoggerConfig {
  sentryDns?: string;

  enableConsoleHttp?: boolean;

  consoleLevel?: 'error' | 'warn' | 'info' | 'http' | 'verbose' | 'debug' | 'silly' | 'emerg' | 'alert' | 'crit' | 'warning' | 'notice';
}

export const getDefaultLoggerConfig = (): LoggerConfig => ({});
