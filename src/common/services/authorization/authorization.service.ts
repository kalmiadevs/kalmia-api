import { TsoaRoute }                                        from '@kalmia/tsoa';
import { BaseAuthAppRequest }                               from '../../interfaces/request.interface';
import { ForbiddenException, InternalServerErrorException } from '../../exceptions/index';
import { App }                                              from '../../../core/index';
import { AuthorizationStrategy }                            from './authorization.strategy';

export class AuthorizationService {

  private strategies: { [key: string]: AuthorizationStrategy | null } = {};

  constructor(private app: App) {}

  addNoOpStrategy(name: string) {
    this.strategies[name] = null;
  }

  addStrategy(name: string, strategy: AuthorizationStrategy) {
    this.strategies[name] = strategy;
  }

  async authorizeRequest(request: BaseAuthAppRequest, security: TsoaRoute.Security[] = []) {
    for (const secMethod of security) {
      if (this.strategies[secMethod.name]) {
        if (!await this.strategies[secMethod.name].authorizeRequest(request, secMethod.scopes, security)) {
          throw new ForbiddenException();
        }
      }  else if (this.strategies[secMethod.name] === null) {
        // noop
      } else {
        throw new InternalServerErrorException('Unknown authorization strategy');
      }
    }
  }
}
