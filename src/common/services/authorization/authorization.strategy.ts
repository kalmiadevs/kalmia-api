import { TsoaRoute }          from '@kalmia/tsoa';
import { BaseAuthAppRequest } from '../../interfaces/index';

/**
 * Authorization strategy.
 */
export abstract class AuthorizationStrategy {
  abstract canBatch(role: string[], operations: string[] | string, params?: any);

  abstract can(role: string[], operation: string, params?: any);

  /**
   * Authorization express request. If not authorized return false or throw exception.
   *
   * @param {BaseAuthAppRequest} request
   * @param {string[]} scopes
   * @param {TsoaRoute.Security[]} security
   */
  abstract async authorizeRequest(request: BaseAuthAppRequest, scopes: string[], security: TsoaRoute.Security[]): Promise<boolean>;
}
