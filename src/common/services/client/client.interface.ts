export interface BaseClientInterface {

  key: string;

  enabled: boolean;

  apiVersion?: string;
  appVersion?: string;

  /**
   * @example "http://studio.dev.salesqueze.com"
   */
  appBaseUrl?: string;
}
