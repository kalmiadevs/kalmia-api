import { App }                                              from '../../../core';
import { BaseClientInterface }                              from './client.interface';
import { BaseAppRequest, BaseAuthAppRequest }               from '../../interfaces';
import { BadRequestException, ServiceUnavailableException } from '../../exceptions';

export class ClientService {

  constructor(private app: App) {

  }

  async attachToRequest(request: BaseAppRequest, clientKey: string) {
    const client = await this.app.clientService.getClient(clientKey);
    if (!client) {
      throw new BadRequestException('Client does not exists.');
    }

    if (!client.enabled) {
      throw new ServiceUnavailableException('Client is disabled.');
    }

    request.ctx.client = client;
    request.ctx.db = this.getClientDb(client.key);
  };

  getClientDb(clientKey: string) {
    return this.app.mongoService.getDb(`client-${ clientKey }`);
  }

  getClient(clientKey: string): Promise<BaseClientInterface> {
    return this.app.mainDb.collection('clients').findOne({ key: clientKey });
  }

  getAllClients(): Promise<BaseClientInterface[]> {
    return this.app.mainDb.collection('clients').find({}).toArray();
  }
}
