import { App }            from '../../../core';
import { BaseAppRequest } from '../../interfaces';
import { TsoaRoute }      from '@kalmia/tsoa';

export class AccountingService {

  constructor(private app: App) {}

  async accountRequest(request: BaseAppRequest, security: TsoaRoute.Security[] = []) {
    // currently not needed
  }
}
