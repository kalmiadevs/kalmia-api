export enum HealthStatus {
  /**
   * The entity is healthy. There are no known issues reported on it or its children (when applicable).
   */
  ok      = 'ok',

  /**
   * The entity has some issues, but it can still function correctly. For example, there are delays, but they do not
   * cause any functional issues yet. In some cases, the warning condition may fix itself without external
   * intervention. In these cases, health reports raise awareness and provide visibility into what is going on. In
   * other cases, the warning condition may degrade into a severe problem without user intervention.
   */
  warning = 'warning',

  /**
   * The entity is unhealthy. Action should be taken to fix the state of the entity, because it can't function properly.
   */
  error   = 'error',
}
