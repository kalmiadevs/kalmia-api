import { App }                    from '../../../core';
import * as express               from 'express';
import { HealthReportInterface }  from './health-report.interface';
import { HealthStatus }           from './health.enum';
import { Container }              from 'typedi';
import { objectToHtml, sendHtml } from '../dashboard/dashboard.service';

export interface ReporterInfo {
  getHealth?: (() => HealthReportInterface | Promise<HealthReportInterface>);
  getDiagnostics?: (() => any | Promise<any>);
}

export class HealthService {

  private static reporters: { [key: string]: ReporterInfo } = {};
  private static reportersKeys: string[] = [];

  static registerReporter(name: string, reporter: ReporterInfo) {
    this.reporters[name] = this.reporters[name] || {};

    this.reporters[name].getDiagnostics = reporter.getDiagnostics;
    this.reporters[name].getHealth = reporter.getHealth;

    this.reportersKeys = Object.keys(this.reporters);
  }

  constructor(private app: App) {
    this.registerPages();
  }

  registerPages() {
    this.app.dashboardService.addProtectedPage('req-info', async (req: express.Request, res: express.Response) => {
      const data = {
        headers: req.headers,
        ips: req.ips,
        ip: req.ip
      };
      if ((req as any).requestedJson) {
        return data;
      } else {
        sendHtml(res, objectToHtml(data));
      }
    });

    this.app.dashboardService.addPage('health', async (req, res: express.Response) => {
      const health = {
        environment  : this.app.env.type,
        appId        : this.app.env.id,
        appName      : this.app.env.name,
        envMode      : this.app.env.mode,
        appVersion   : this.app.env.version,
        build        : this.app.env.build,
        dbIsConnected: this.app.mongoService.isConnected(),
        serverTime   : `${ new Date() }`,
        timestamp    : +new Date(),
        reports      : {} as { [key: string]: HealthReportInterface }
      };

      await Promise.all(HealthService.reportersKeys
        .filter(name => !!HealthService.reporters[name].getHealth)
        .map(name =>
          Promise.resolve(HealthService.reporters[name].getHealth())
            .then(s => {
              health.reports[name] = s;
            }).catch(e => {
            health.reports[name] = {
              report: e.message,
              status: HealthStatus.error
            };
          })
        ));

      if (req.requestedJson) {
        return health;
      } else {
        sendHtml(res, objectToHtml(health));
      }
    });

    this.app.dashboardService.addProtectedPage('diagnostics', async (req, res: express.Response) => {
      const diagnostics: { reports: { [key: string]: any } } = { reports: {} };

      await Promise.all(HealthService.reportersKeys
        .filter(name => !!HealthService.reporters[name].getDiagnostics)
        .map(name =>
          Promise.resolve(HealthService.reporters[name].getDiagnostics())
            .then(s => {
              diagnostics.reports[name] = s;
            }).catch(e => {
            diagnostics.reports[name] = {
              error: e.message
            };
          })
        ));

      if (req.requestedJson) {
        return diagnostics;
      } else {
        sendHtml(res, objectToHtml(diagnostics));
      }
    });
  }
}

export function Health() {
  // tslint:disable-next-line:only-arrow-functions
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {

    HealthService.registerReporter(target.constructor.name, {
      getHealth: () => Container.get(target.constructor.name)[propertyKey]()
    });
  };
}

export function Diagnostics() {
  // tslint:disable-next-line:only-arrow-functions
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {

    HealthService.registerReporter(`${ target.constructor.name }.${ propertyKey }`, {
      getDiagnostics: () => Container.get(target.constructor)[propertyKey]()
    });
  };
}
