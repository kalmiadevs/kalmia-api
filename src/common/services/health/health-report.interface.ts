import { HealthStatus } from './health.enum';

export interface HealthReportInterface {
  status: HealthStatus;
  report?: any;
}
