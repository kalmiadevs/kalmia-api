import { BaseAppRequest, BaseAuthAppRequest } from '../../interfaces/index';
import { TsoaRoute }                          from '@kalmia/tsoa';

/**
 * Authorization strategy.
 */
export abstract class ConnectorStrategy {

  /**
   * Attach db to express request. If can not attach return false or throw exception.
   *
   * @param {BaseAuthAppRequest} request
   */
  abstract async connect(request: BaseAppRequest, scopes: string[], security: TsoaRoute.Security[]): Promise<boolean>;
}
