/**
 * Authorization strategy.
 */
import { ConnectorStrategy }                  from '../connector.strategy';
import { BaseAppRequest, BaseAuthAppRequest } from '../../../interfaces';
import { App }                                from '../../../../core';
import { TsoaRoute }                          from '@kalmia/tsoa';
import { BadRequestException }                from '../../../exceptions';

export class ConnectorDefaultStrategy extends ConnectorStrategy {

  constructor(private app: App) {
    super();
  }

  /**
   * Attach db to express request. If can not attach return false or throw exception.
   *
   * @param {BaseAuthAppRequest} request
   */
  async connect(request: BaseAppRequest, scopes: string[], security: TsoaRoute.Security[]) {
    const clientKey = request.headers['x-client-key'] as string || request.query['__x-client-key'];
    if (!clientKey) {
      throw new BadRequestException('Missing "X-Client-Key" header.');
    }
    await this.app.clientService.attachToRequest(request, clientKey);

    return true;
  };
}
