import { TsoaRoute }                                        from '@kalmia/tsoa';
import { BaseAppRequest, BaseAuthAppRequest }               from '../../interfaces/request.interface';
import { ForbiddenException, InternalServerErrorException } from '../../exceptions/index';
import { App }                                              from '../../../core/index';
import { ConnectorStrategy }                                from './connector.strategy';
import { ConnectorDefaultStrategy }                         from './strategies/connector-default.strategy';

export class ConnectorService {

  private strategies: { [key: string]: ConnectorStrategy } = {};

  constructor(private app: App) {
    this.addStrategy('client', new ConnectorDefaultStrategy(this.app));
  }

  addStrategy(name: string, strategy: ConnectorStrategy) {
    this.strategies[name] = strategy;
  }

  async connectRequest(request: BaseAppRequest, security: TsoaRoute.Security[] = []) {
    for (const secMethod of security) {
      if (this.strategies[secMethod.name]) {
        if (!await this.strategies[secMethod.name].connect(request, secMethod.scopes, security)) {
          throw new ForbiddenException('Connector security strategy refused your request.');
        }
      }  else if (this.strategies[secMethod.name] === null) {
        // noop
      } else {
        // throw new InternalServerErrorException('Unknown authorization strategy');
      }
    }
  }
}
