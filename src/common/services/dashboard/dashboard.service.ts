import { App }            from '../../../core';
import * as express       from 'express';
import { BaseAppRequest } from '../../interfaces/request.interface';

export function objectToHtml(json: any): string {
  if (typeof json !== 'string') {
    json = JSON.stringify(json, undefined, 2);
  }
  json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
  const html = json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
    let cls = 'number';
    if (/^"/.test(match)) {
      if (/:$/.test(match)) {
        cls = 'key';
      } else {
        cls = 'string';
      }
    } else if (/true|false/.test(match)) {
      cls = 'boolean';
    } else if (/null/.test(match)) {
      cls = 'null';
    }
    return '<span class="' + cls + '">' + match + '</span>';
  });
  return `<style>
pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; }
.string { color: green; }
.number { color: darkorange; }
.boolean { color: blue; }
.null { color: magenta; }
.key { color: red; }
</style><pre>${ html }</pre>`;
}

export function sendLoginPage(response: express.Response, { redirectTo, msg }: { redirectTo?: string, msg?: string } = {}) {
  sendHtml(response, `
<div class="row"><div class="column column-50 column-offset-25">
<h2>System login</h2>
            
               <form method="post" action="/_/login?redirect=${ redirectTo && encodeURIComponent(redirectTo) || '/' }">
                Username:<br>
                <input type="text" name="username">
                <br>
                Password:<br>
                <input type="password" name="password">
                <br><p>${ JSON.stringify(msg) }</p><br>
                <input type="submit" value="Submit">
              </form></div></div>`);
}

export function sendHtml(response: express.Response, html: string) {
  response.set('Content-Type', 'text/html');
  response.send(new Buffer(`<html>
<!--
    This is an HTML rendering of the Kalmia core API response.

    We're showing you HTML so you get convenient syntax highlighting and links,
    because we think you're seeing this in a Web browser, based on your
    'Accept: text/html' header.

    If you're using a client that isn't a Web browser, you probably wanted pure
    JSON. In that case, set the header 'Accept: */*' or 'Accept:
    application/json' on your request.
-->
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
<link rel="stylesheet" href="//cdn.rawgit.com/necolas/normalize.css/master/normalize.css">
<link rel="stylesheet" href="//cdn.rawgit.com/milligram/milligram/master/dist/milligram.min.css">
  <body>
     ${ html }
  </body>
</html>`));
}

export function sendDashHtml(response: express.Response, html: string) {
  sendHtml(response, `[nav bar]
               <hr>
               ${ html }`);
}

export class DashboardService {

  started: boolean;

  pagesStack: { page: string, processor: any }[] = [];

  validateToken = (token: string) => false;
  canAccessDash = async (req: express.Request) => {
    if (this.app.env.isDev) {
      return true;
    }
    try {
      let token = req.cookies && req.cookies.jwt;
      if (token && this.validateToken(token)) {
        return true;
      }

      token = await this.getDashJwtToken(req.body.username, req.body.password);
      if (!token) {
        return false;
      }

      req.res.cookie('jwt', token, { maxAge: 30 * 60 * 1000, httpOnly: true });
      return true;
    } catch (e) {
      console.error({ msg: JSON.stringify(e.message), type: 'error' });
      return false;
    }
  };
  getDashJwtToken = async (identifier: string, secret: string) => void 0 as string | undefined;

  constructor(private app: App) {
    app.routerBootstrap.registerAfterInit(() => {
      this.start();
    });
  }

  start() {
    this.started = true;
    this.pagesStack.forEach(x => {
      this.app.expressApp.all('/_/' + x.page, x.processor);
      this.app.expressApp.all('/api/_/' + x.page, x.processor);
    })
  }

  enableIndex() {
    this.app.routerBootstrap.registerAfterInit(() => {
      this.app.expressApp.get('/', (req, res) => {
        if (req.get('Content-Type') === 'application/json') {
          res.json({
            docsJson  : `_/api.json`,
            health    : `_/health`,
            apiVersion: `v${this.app.env.version.split('.').shift()}`,
            build     : this.app.env.build,
          });
        } else {
          sendHtml(res, `
<div class="row"><div class="column column-50 column-offset-25">
<h1>Api - ${ this.app.env.name }</h1>

<blockquote>
  <p><em>Welcome to Kalmia API core service.</em></p>
</blockquote>

<p>Version: ${this.app.env.version.split('.').shift()}</p>
<p>Build: ${this.app.env.build && this.app.env.build.date}</p>

<h3>Core index</h3>
<ul>
  <li><a href="_/api">Api documentation</a></li>
  <li><a href="_/api.json">Api documentation (JSON)</a></li>
  <br>
  <li><a href="_/health">Api health</a></li>
  <li><a href="_/req-info">Req info</a></li>
  <li><a href="_/diagnostics">Diagnostics</a></li>
  <br>
  <li><a href="_/queue">Queue</a></li>
  <li>(not supported) <a href="_/logs">Logs</a></li>
  <li>(not supported) <a href="_/scripts">Scripts</a></li>
</ul>
</div></div>
`);
        }
      });
      this.app.expressApp.all('/_/login', async (req, res) => {
        try {
          if (await this.canAccessDash(req)) {
            return req.res.redirect(req.query.redirect || '/');
          } else {
            sendLoginPage(res);
          }
        } catch (e) {
          return sendLoginPage(res, { msg: e.message });
        }
      });
    });
  }

  getProtectedPageMiddleware() {
    return async (req: express.Request, res: express.Response, next) => {
      try {
        if (await this.canAccessDash(req)) {
          next();
        } else {
          sendLoginPage(res);
        }
      } catch (e) {
        sendLoginPage(res, { msg: e.message });
      }
    }
  }

  addProtectedPage(page: string, handler: (request: BaseAppRequest & { requestedJson: boolean }, response: express.Response, next: any) => any) {
    return this.addPage(page, handler, { protected: true });
  }

  addPage(page: string, handler: (request: BaseAppRequest & { requestedJson: boolean }, response: express.Response, next?: any) => any | Promise<any>, options: { protected?: boolean /*protocol?: 'GET' | 'POST' | 'DELETE'*/ } = {}) {
    const processor = async (request: BaseAppRequest, response: express.Response, next: any) => {
      try {
        (request as any).requestedJson = (request as any).get('Content-Type') === 'application/json';
        if (!options.protected || (await this.canAccessDash(request))) {
          await Promise.resolve(handler(request as any, response, next))
            .then(data => {
              if (data !== undefined) {
                response.status(200).json(data);
              }
            })
            .catch(e => next(e));
        } else {
          if ((request as any).requestedJson) {
            response.status(403).json({ status: 403 });
          } else {
            sendLoginPage(response, { redirectTo: '/_/' + page });
          }
        }
      } catch (e) {
        sendLoginPage(response, { redirectTo: '/_/' + page, msg: e.message });
      }
    };

    if (this.started) {
      this.app.expressApp.all('/_/' + page, processor);
      this.app.expressApp.all('/api/_/' + page, processor);
    } else {
      this.pagesStack.push({
        processor, page
      });
    }
  }
}
