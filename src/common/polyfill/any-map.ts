/**
 * Map that lets you any key type not just primitives.
 *
 * Note! Iteration will throw 'safe keys' you must convert them or improve this class with custom MapIterator.
 */
export class AnyMap<K, V> extends Map<any, V> {

  static toSafeKey(key: any) {
    return key ? JSON.stringify(key) : key;
  }

  static safeKeyToKey(safeKey: any) {
    return safeKey ? JSON.parse(safeKey) : safeKey;
  }

  set(key: K, value: V): this {
    return super.set(AnyMap.toSafeKey(key), value);
  }

  get(key: K): V | undefined {
    return super.get(AnyMap.toSafeKey(key));
  }

  has(key: K): boolean {
    return super.has(AnyMap.toSafeKey(key));
  }

  // We could also mock MapIterator but for now this is good enough.
  // entries(): MapIterator {
  //   const i = super.entries();
  //   const orig = i.next;
  //   const self = this;
  //   i.next = (): IteratorResult<K, V> => {
  //     const x = orig.bind(self)();
  //     x[0] = self.safeKeyToKey(x[0]);
  //     return x;
  //   };
  //   return i;
  // }
}
