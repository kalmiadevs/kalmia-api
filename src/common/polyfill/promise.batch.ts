/**
 * EXAMPLE:
 * batchPromises(2, [1,2,3,4,5], i => new Promise((resolve, reject) => {
 *   // The iteratee will fire after each batch resulting in the following behaviour:
 *   // @ 100ms resolve items 1 and 2 (first batch of 2)
 *   // @ 200ms resolve items 3 and 4 (second batch of 2)
 *   // @ 300ms resolve remaining item 5 (last remaining batch)
 *   setTimeout(() => {
 *     resolve(i);
 *   }, 100);
 * }))
 *  .then(results => {
 *   console.log(results); // [1,2,3,4,5]
 * });
 *
 *
 * @param {number} batchSize
 * @param {any[]} thenArr
 * @param {(x: any) => Promise<any>} fn
 * @returns {Promise<any[]>}
 */
export function batchPromises(batchSize: number, thenArr: any[], fn: (x: any) => Promise<any>) {
  return Promise.resolve(thenArr)
    .then((arr) => {
      return arr
        .map((_, i) => {
          return i % batchSize ? [] : arr.slice(i, i + batchSize);
        })
        .map((group) => {
          return (res: any[]) => {
            return Promise.all(group.map(fn)).then(function (r) {
              return res.concat(r);
            });
          }
        })
        .reduce((chain, work) => {
          return chain.then(work);
        }, Promise.resolve([]));
    });
}
