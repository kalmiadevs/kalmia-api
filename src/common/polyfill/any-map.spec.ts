import { AnyMap } from './any-map';

describe('CAnyMap', () => {
  it('set', () => {
    const m = new AnyMap<string[] | string, number>();
    m.set(['a'], 1);
    m.set(['a'], 2);
    m.set(undefined, 3);
    m.set(null, 4);
    m.set('b', 5);
    m.set('b', 6);

    expect(m.size).toBe(4);
  });

  // it('entries', () => {
  //   const m = new AnyMap<string[], number>();
  //   m.set(['a'], 1);
  //   const debug = m.entries();
  //   for (const [k, v] of m.entries()) {
  //     expect(k).toMatchObject(['a']);
  //   }
  // });
});
