export class ArraySet<T> extends Set<T> {

  concat(items: Array<T>): ArraySet<T> {
    for (const item of items) {
      this.add(item);
    }
    return this;
  }

  toArray(): Array<T> {
    return Array.from(this);
  }

}
