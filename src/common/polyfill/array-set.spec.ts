import { ArraySet } from './array-set';

describe('ArraySet', () => {
  it('concat', () => {
    const arraySet = new ArraySet<number>([1, 2, 2, 3, 1, 5]);
    arraySet.concat([1, 2, 3, 4, 5, 6, 7, 8, 3, 9]);
    expect(arraySet.size).toBe(9);
  });

  it('toArray', () => {
    const itemsArray = [4, 6, 7, 1, 9, 5, 2, 8];
    const arraySet = new ArraySet<number>(itemsArray);
    expect(arraySet.toArray()).toEqual(itemsArray)
  });
});
