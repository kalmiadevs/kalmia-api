export interface Constructable<T> {
  new(): T;
}
