import * as express            from 'express';
import { Db }                  from 'mongodb';
import { BaseClientInterface } from '../services/client/client.interface';

export interface BaseAppRequestCtx {
  id: string;

  client: BaseClientInterface;

  db: Db;

  error?: any;
  _preventDefaultResponse?: boolean;
}

export interface BaseAppRequest extends express.Request {
  ctx: BaseAppRequestCtx;

  preventDefaultResponse: () => void;
  res: express.Response;
  _routeWhitelists: {
    body?: string[],
    res?: string[],
  };
}

export interface BaseAuthAppRequestCtx extends BaseAppRequestCtx {

  token: {
    exp: number;
    iss: string;
    sub: string;
    iat: number;
  };

  /**
   * Merged from user & groups.
   */
  roles: string[];
}

export interface BaseAuthAppRequest extends BaseAppRequest {
  ctx: BaseAuthAppRequestCtx
}
