export enum ComponentGroup {
  job        = 'job',
  task       = 'task',
  cron       = 'cron',
  service    = 'svc',
  controller = 'ctr',
  parser     = 'parser',
  core       = 'core',
  app        = 'app'
}
