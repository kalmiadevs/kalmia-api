import { ComponentGroup }                      from './component-group.enum';
import { isString }                            from 'lodash';
import { LoggerInstance }                      from 'winston';
import { Container }                           from 'typedi';
import { Constructable }                       from '../interfaces';
import { App }                                 from '../../core';
import { HealthReportInterface, HealthStatus } from '../services/health';

export type ComponentNameArg = string | { name: string, group: ComponentGroup | string };

export interface ComponentConstructable<T> extends Constructable<T> {
  new(name: ComponentNameArg): T;
}

/**
 * Do not forget to call init() for extra features.
 */
export abstract class Component {

  app: App;

  /**
   * Logger instance intended only for this service.
   */
  logger: LoggerInstance;

  /**
   * Service name in kebab-case format.
   */
  name: string;

  constructor(name: ComponentNameArg, options: { category?: ComponentGroup | string } = {}) {
    this.app = Container.get(App);

    const serviceGroup: string = !isString(name) && name.group || ComponentGroup.app;
    this.name = (isString(name) ? name : name.name).trim()
      .replace(/([a-z])([A-Z])/g, '$1-$2')
      .replace(/\W/g, m => /[À-ž]/.test(m) ? m : '-')
      .replace(/^-+|-+$/g, '')
      .toLowerCase();

    this.logger = this.app.loggerFactory.create(options.category || ComponentGroup.app, [`${ this.name }`, serviceGroup]);

    // const endsWith = ['-service', '-controller', '-seed'].find(x => this.name.endsWith(x));
    // if (endsWith) {
    //   this.name = this.name.substring(0, this.name.lastIndexOf(endsWith));
    // }
  }

  /**
   * Used for creating instances and checking for all services (goal: prevent undefined at runtime).
   */
  getHealth(): HealthReportInterface | Promise<HealthReportInterface> {
    return { status: HealthStatus.ok };
  }
}
