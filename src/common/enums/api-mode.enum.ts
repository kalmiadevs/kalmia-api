export enum ApiMode {
  Worker = 'worker',
  Script = 'script',
  Server = 'server',
  Test   = 'test'
}
