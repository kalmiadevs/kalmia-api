export enum ApiEnvironment {
  /**
   * Normal operation (this includes dev, stage, prod servers).
   * Do not confuse deployed dev server for dev environment! Deployed dev server is running in prod mode!
   */
  Prod  = 'prod',

  /**
   * Special testing mode, some functionality may be simulated.
   */
  QA    = 'qa',

  /**
   * Local development instance.
   */
  Dev   = 'dev'
}
