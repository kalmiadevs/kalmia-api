/*================================*/
// AUTO GENERATED - DO NOT CHANGE  /
// use: npm run update             /
/*================================*/

/* tslint:disable */
import 'reflect-metadata';
import { Container }                                                          from 'typedi';
import { RouteHandler, RouteValidator, RouteSecurity, App }                   from '@kalmia/api';

{{#if canImportByAlias}}
  import { Controller, ValidateParam, FieldErrors, ValidateError, TsoaRoute } from '@kalmia/tsoa';
{{else}}
  import { Controller, ValidateParam, FieldErrors, ValidateError, TsoaRoute } from '../../../src';
{{/if}}
{{#if iocModule}}
import { iocContainer } from '{{iocModule}}';
{{/if}}
{{#each controllers}}
import { {{name}} } from '{{modulePath}}';
{{/each}}

const models: TsoaRoute.Models = {
    {{#each models}}
    "{{@key}}": {
        {{#if enums}}
        "enums": {{{json enums}}},
        {{/if}}
        {{#if properties}}
        "properties": {
            {{#each properties}}
            "{{@key}}": {{{json this}}},
            {{/each}}
        },
        {{/if}}
        {{#if additionalProperties}}
        "additionalProperties": {{{json additionalProperties}}},
        {{/if}}
    },
    {{/each}}
};

export function RegisterRoutes(app: App) {
    {{#each controllers}}
    {{#each actions}}
        app.expressApp.{{method}}(['/api{{fullPath}}', '{{fullPath}}'],
            {{#if security.length}}
            RouteSecurity.getAuthenticateMiddleware(app, {{json security}}),
            {{/if}}
            function (request: any, response: any, next: any) {
            const args = {
                {{#each parameters}}
                    {{@key}}: {{{json this}}},
                {{/each}}
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = RouteValidator.getValidatedArgs(args, request, models);
            } catch (err) {
                return next(err);
            }

            {{#if ../../iocModule}}
            const controller = iocContainer.get<{{../name}}>({{../name}});
            if (typeof controller['setStatus'] === 'function') {
                (<any>controller).setStatus(undefined);
            }
            {{else}}
            // const controller = new {{../name}}();
            const controller = Container.get({{../name}});
            {{/if}}


            const promise = controller.{{name}}.apply(controller, validatedArgs);
            RouteHandler.handlePromise(controller, promise, request, response, next)
              .catch(e =>
                console.error(JSON.stringify({
                  type: 'error',
                  source: 'RouteHandler.handlePromise',
                  message: e.message,
                  stack: e.stack
                })));
        });
    {{/each}}
    {{/each}}
}
